//
// io.h
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#ifndef YT_IO_H
#define YT_IO_H

#include <stdlib.h>
#include <unicode/utext.h>
#include <unicode/ustdio.h>

UText* yt_file_slurp(char const* filename);

#endif /* YT_IO_H */
