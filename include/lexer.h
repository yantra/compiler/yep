//
// lexer.h
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#ifndef YT_LEXER_H
#define YT_LEXER_H

#include <sys/types.h>
#include <unicode/utypes.h>
#include <unicode/ustring.h>
// #include <libhandler.h>
#include "token.h"

#ifndef YT_BUF_SIZE
#define YT_BUF_SIZE 1024
#endif

/*
 * Used by UChar32-returning funcs to signify an error
 */
#define YT_LEXER_INVALID -1

typedef struct {
  char *error;
  	int stash;
  int lineno;
  off_t offset;
  UText* source;
  char const* filename;
  yt_token_t tok;
  char buf[YT_BUF_SIZE];
} yt_lexer_t;

/*
 * Convert hex digit `c` to a base 10 int,
 * returning -1 on failure.
 */
static inline int hex(UChar32 const c) {
  switch (c) {
  case u'0': return 0;
  case u'1': return 1;
  case u'2': return 2;
  case u'3': return 3;
  case u'4': return 4;
  case u'5': return 5;
  case u'6': return 6;
  case u'7': return 7;
  case u'8': return 8;
  case u'9': return 9;
  case u'a':
  case u'A': return 10;
  case u'b':
  case u'B': return 11;
  case u'c':
  case u'C': return 12;
  case u'd':
  case u'D': return 13;
  case u'e':
  case u'E': return 14;
  case u'f':
  case u'F': return 15;
  default: return YT_LEXER_INVALID;
  }
}

yt_token_kind_t yt_scan(yt_lexer_t* self);

yt_lexer_t yt_lexer_init(UText* source, char const* filename);

#endif /* YT_LEXER_H */
