//
// token.h
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#ifndef YT_TOKEN_H
#define YT_TOKEN_H

#include <unicode/utypes.h>
#include <unicode/utext.h>
#include <unicode/ustdio.h>

// Need strlen for converting from c to u
#include <string.h>

// #define T(name, repr) YT_TOK_##name,

/*
 * Tokens.
 */
// T(SIGIL_OBJ,  "*") // Currently toying no sigil for objects
#define YT_TOKENS \
  T(ILLEGAL, "illegal") \
  T(EOS, "end-of-source") \
  T(ID, "identity") \
  T(INT, "int") \
  T(FLOAT, "float") \
  T(HEX, "hex") \
  T(STRING, "string") \
  T(SIGIL_BOOL, "!") \
  T(SIGIL_COLL, "#") \
  T(SIGIL_STR,  "$") \
  T(SIGIL_NUM,  "%") \
  T(SIGIL_ROUT, "&") \
  T(SIGIL_DYN,  "_") \
  T(SIGIL_THR,  "~") \
  T(OP_COMMA, ",") \
  T(OP_LASS, "<-") \
  T(OP_RASS, "->")  \
  T(OP_MLASS, "<~") \
  T(OP_MRASS, "~>") \
  T(OP_SLASS, "*>") \
  T(OP_SRASS, "<*") \
  T(OP_DLASS, "<|") \
  T(OP_DRASS, "|>") \
  T(OP_HLASS, "<$") \
  T(OP_HRASS, "$>") \
  T(OP_OLASS, "<[") \
  T(OP_ORASS, "]>") \
  T(OP_PLUS, "+") \
  T(OP_DPLUS, "++") \
  T(OP_MIN, "-") \
  T(OP_DMIN, "--") \
  T(OP_STAR, "*") \
  T(OP_DSTAR, "**") \
  T(OP_SLASH, "/") \
  T(OP_DSLASH, "//") \
  T(OP_SLASHSTAR, "/*") \
  T(OP_STARSLASH, "*/") \
  T(OP_PSIGN, "%") \
  T(OP_DPSIGN, "%%") \
  T(OP_DOT, ".") \
  T(OP_DDOT, "..") \
  T(OP_TDOT, "...") \
  T(OP_LT, "<") \
  T(OP_LTLT, "<<") \
  T(OP_GT, ">") \
  T(OP_GTGT, ">>") \
  T(OP_GTGTEQ, ">>=") \
  T(OP_EQLTLT, "=<<") \
  T(OP_EQCLEQ, "=:=") \
  T(OP_EQGTGT, "=>>") \
  T(OP_LTLTEQ, "<<=") \
  T(OP_EQEQ, "==") \
  T(OP_EXEQ, "!=") \
  T(OP_EQEQEQ, "===") \
  T(OP_EQEXEQ, "!==") \
  T(OP_EQLT, "=<") \
  T(OP_GTEQ, ">=") \
  T(OP_GTMINGT, ">->") \
  T(OP_GTEQGT, ">=>") \
  T(OP_LTMINLT, "<-<") \
  T(OP_LTEQLT, "<=<") \
  T(OP_GTMIN, ">-") \
  T(OP_MINLT, "-<") \
  T(OP_GTGTMIN, ">>-") \
  T(OP_MINLTLT, "-<<") \
  T(OP_EQEQGT, "==>") \
  T(OP_LTEQEQ, "<==") \
  T(OP_BSLASH, "\\") \
  T(OP_DBSLASH, "\\\\") \
  T(OP_CL, ":") \
  T(OP_CLCL, "::") \
  T(OP_CLCLCL, ":::") \
  T(OP_CLEQ, ":=") \
  T(OP_DOTEQ, ".=") \
  T(OP_AT, "@") \
  T(OP_ATAT, "@@") \
  T(OP_AMP, "&") \
  T(OP_DAMP, "&&") \
  T(OP_PIPE, "|") \
  T(OP_DPIPE, "||") \
  T(OP_BSLASHSLASH, "\\/") \
  T(OP_SLASHBSLASH, "/\\") \
  T(OP_QMARK, "?") \
  T(OP_DQMARK, "??") \
  T(OP_EXMARK, "!") \
  T(OP_DEXMARK, "!!") \
  T(LBRACK, "[") \
  T(RBRACK, "]") \
  T(LBRACE, "{") \
  T(RBRACE, "}") \
  T(LPAREN, "(") \
  T(RPAREN, ")") \
  T(NUMBRACK, "#[") \
  T(BRACKNUM, "]#") \
  T(BRACEDOT, "{.") \
  T(DOTBRACE, ".}") \
  T(LTGT, "<>") \
  T(COMMA, ",") \
  T(SEMICL, ";")

#define YT_TOKEN_COUNT 99

/*
 * The maximum size of string literals
 */
#define STRING_BUFFER_MAX 128

typedef enum {
#define T(name, str) YT_TOKEN_##name,
YT_TOKENS
#undef T
} yt_token_kind_t;


/*
 * An array of token strings as c strings
 */
char* yt_token_ctoks[YT_TOKEN_COUNT];

typedef struct {
  yt_token_kind_t kind;
  union {
    UChar str[STRING_BUFFER_MAX];
    int num;
    float flt;
  } val;
} yt_token_t;


/*
 * Return the string associated with the given token `kind`.
 */
UText const yt_token_type_string(yt_token_kind_t kind);

/*
 * Inspect the given token, outputting debugging info to stdout
 */
void yt_token_inspect(yt_token_t* token);

#endif /* YT_TOKEN_H */
