//
// yep.h
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//

#ifndef YT_H
#define YT_H
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unicode/ustdio.h>

// For isatty
// #include <unistd.h>

// Not sure if I will use this yet
// #include <libhandler.h>//

#include "io.h"
#include "lexer.h"
// #include "token.h"

/*
 * Yantra version.
 */
#define YT_VERSION "0.0.1"

#endif /* YT_H */
