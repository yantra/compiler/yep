//
// io.c
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#include "io.h"

UText* yt_file_slurp(char const* filename) {
  UFILE* file = u_fopen(filename, "r", NULL, "UTF-8");
  if (file == NULL) {
    puts("Cannot open file\n");
    exit(EXIT_FAILURE);
  }

  // Apparently you have to use C stdio for file size
  FILE* rawfile = u_fgetfile(file);
  size_t fsize = (fseek(rawfile, 0, SEEK_END), ftell(rawfile));
  // rewind with icu api
  u_frewind(file);

  // Allocate memory for string plus terminator
  UChar* ustr = (UChar*) malloc(sizeof(UChar) * fsize);

  /* Read the string into memory */
  for (size_t cursor = 0; cursor < fsize; cursor++) {
    UChar nextchar = u_fgetc(file);
    if (nextchar == U_EOF) break;
    ustr[cursor] = nextchar;
  }

  u_fflush(file);
  u_fclose(file);

  /* Convert to UText for consumption */
  UErrorCode err = U_ZERO_ERROR;
  UText* retval = utext_openUChars(NULL, ustr, fsize, &err);
  if (U_FAILURE(err)) {
    printf("Error converting to UText: %s", u_errorName(err));
    exit(EXIT_FAILURE);
  }
  return retval;
}

