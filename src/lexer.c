//
// lexer.c
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#include "lexer.h"

/*
 * Moves stash to next character in source and when set to
 * debug mode, it prints the stashed character to the console.
 */
/* #ifdef DEBUG_LEXER */
#define NEXT \
  (self->stash = UTEXT_NEXT32(self->source)\
    , fprintf(stderr, "%d: '%c'\n", self->stash, self->stash)\
    , self->offset++\
    , self->stash)
/* #else */
/* #define NEXT (self->stash = self->source[self->offset++]) */
/* #endif */


/*
 * Undo the previous character by decrementing the offset
 */
#define UNDO (self->offset--, self->stash = UTEXT_PREVIOUS32(self->source))

/*
 * Set the token kind that the parser switches over
 */
#define TOKEN(t) (self->tok.kind = YT_TOKEN_##t)

/*
 * Tests the next character and keeps it or backs up
 */
#define IS_IT(this) (this == NEXT ? this : (UNDO, 0))

/*
 * Sets the error and sets the token kind to ILLEGAL
 */
#define ERROR(msg) (self->error = msg, TOKEN(ILLEGAL))

/*
 * Initializes the lexer object.
 * The error field holds a potential error message
 * The source field holds the
 */
yt_lexer_t yt_lexer_init(UText* source, char const* filename) {
  yt_lexer_t ret = {
    .error = NULL,
    .source = source,
    .filename = filename,
    .lineno = 1,
    .offset = 0
  };
  return ret;
}

static UChar32 hex_literal(yt_lexer_t* self) {
  int a = hex(NEXT); int b = hex(NEXT);
  return (a > YT_LEXER_INVALID && b > YT_LEXER_INVALID)
    ? (a << 4 | b)
    : (ERROR("string hex literal \\x contains invalid digits"), YT_LEXER_INVALID);
}

static yt_token_kind_t scan_string(yt_lexer_t* self, UChar32 quote) {
  UChar32 c = 0; // the current codepoint that is being assessed
  size_t len = 0; // the current length of the string
  UErrorCode status; // the status of converting to u16 & utext

  UChar32 buffer[STRING_BUFFER_MAX] = {0};
  UChar   string[STRING_BUFFER_MAX] = {0};

  TOKEN(STRING);

  while ((c = NEXT) != quote) {
    switch (c) {
    case u'\n': self->lineno++; break;
    case u'\\':
      switch (c = NEXT) {
      case u'a': c = u'\a'; break;
      case u'b': c = u'\b'; break;
      case u'e': c = u'\e'; break;
      case u'f': c = u'\f'; break;
      case u'n': c = u'\n'; break;
      case u'r': c = u'\r'; break;
      case u't': c = u'\t'; break;
      case u'v': c = u'\v'; break;
      case u'x':
        if ((c = hex_literal(self)) == YT_LEXER_INVALID)
          return ERROR("Failed to scan hex literal");
      }
    }
    buffer[len++] = c;
  }

  buffer[len++] = u'\0';

  status = U_ZERO_ERROR;
  u_strFromUTF32(string, len, NULL, buffer, len, &status);
  if (U_FAILURE(status)) return ERROR("Failed to convert char from U32 to U16");
  u_strcpy(self->tok.val.str, string);
  return (self->tok.val.str[0] != 0
          ? TOKEN(STRING)
          : ERROR("Failed to convert buffer to UChar16's"));
}

yt_token_kind_t yt_scan(yt_lexer_t* self) {
  UChar32 c;

scan:
  switch (c = NEXT) {
  case u' ':
  case u'\t': goto scan;
  case u'(': return TOKEN(LPAREN);
  case u')': return TOKEN(RPAREN);
  case u'{': return TOKEN(LBRACE);
  case u'}': return TOKEN(RBRACE);
  case u'[': return TOKEN(LBRACK);
  case u']': return TOKEN(RBRACK);
  case u',': return TOKEN(COMMA);
  case u'%': return TOKEN(OP_PSIGN);
  case u'.': return TOKEN(OP_DOT);
  case u'<':
    switch (c = NEXT) {
    case u'>': return TOKEN(LTGT);
    case u'-': return TOKEN(OP_LASS);
    case u'~': return TOKEN(OP_MLASS);
    case u'*': return TOKEN(OP_SLASS);
    case u'|': return TOKEN(OP_DLASS);
    case u'$': return TOKEN(OP_HLASS);
    case u'[': return TOKEN(OP_OLASS);
    default: return (UNDO, TOKEN(OP_LT));
    }
  case u'-':
    switch (c = NEXT) {
    case u'>': return TOKEN(OP_RASS);
    case u'-': return TOKEN(OP_DMIN);
    default:   return TOKEN(OP_MIN);
    }
  case u'"':
  case u'\'':
    printf("Scanning a string...\n");
    return scan_string(self, c);
  default:
    printf("Oh no! :(\nThis is what you tried to enter: %d\n", c);
    return TOKEN(ILLEGAL);
  }
}
