//
// token.h
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#include <assert.h>
#include "token.h"

char* yt_token_ctoks[YT_TOKEN_COUNT] = {
#define T(name, str) str,
YT_TOKENS
#undef T
};


/*
 * Converts a char token to a utext token for consumption
 */
UText const ctok_to_utok(char* ctok, size_t clen) {
  UErrorCode err = U_ZERO_ERROR;
  UText retval = UTEXT_INITIALIZER;
  utext_openUTF8(&retval, ctok, clen, &err);
  if (U_FAILURE(err))
    printf("Error converting to UText: %s", u_errorName(err));
  return retval;
}

UText const yt_token_type_string(yt_token_kind_t kind) {
  assert(kind <= YT_TOKEN_COUNT);
  char* cstr = yt_token_ctoks[kind];
  return ctok_to_utok(cstr, strlen(cstr));
}

void yt_token_inspect(yt_token_t* token) {
  u_printf("\e[90m%s\e[0m", yt_token_ctoks[token->kind]);
  switch (token->kind) {
    case YT_TOKEN_INT:
      u_printf(" \e[36m%d\e[0m\n", token->val.num); break;
    case YT_TOKEN_FLOAT:
      u_printf(" \e[36m%f\e[0m\n", token->val.flt); break;
    case YT_TOKEN_STRING:
      u_printf(" \e[36m%S\e[0m\n", token->val.str); break;
    case YT_TOKEN_ID:
      u_printf(" \e[36m%S\e[0m\n", token->val.str); break;
    default:
      u_printf("\n");
  }
}
