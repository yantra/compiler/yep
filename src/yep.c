//
// yep.c
//
// Copyright (c) 2017 Jacob Russo <madcap.russo@gmail.com>
//
#include "yep.h"

// --ast
static bool ast = false;

// --tokens
static bool tokens = false;

UFILE *file;
UChar c;

/*
 * Output usage information.
 */
void usage() {
  fprintf(stderr,
    "\n  Usage: yep [options] [file]"
    "\n"
    "\n  Options:"
    "\n"
    "\n    -A, --ast       output ast to stdout"
    "\n    -T, --tokens    output tokens to stdout"
    "\n    -h, --help      output help information"
    "\n    -V, --version   output yep version"
    "\n"
    "\n  Examples:"
    "\n"
    "\n    $ yep < some.yt"
    "\n    $ yep some.yt"
    "\n    $ yep --tokens some.yt"
    "\n    $ yep"
    "\n"
    "\n"
    );
  exit(1);
}

/*
 * Output yep version.
 */
void version() {
  printf("%s\n", YT_VERSION);
  exit(0);
}

/*
 * Parse arguments.
 */
char** parse_args(int* argc, char* argv[*argc+1]) {
  char* arg = {0}; // argument cursor
  char** args = argv;

  for (unsigned i = 0; i < *argc; i++) {
    arg = args[i];
    if (!strcmp("-h", arg) || !strcmp("--help", arg))
      usage();
    else if (!strcmp("-V", arg) || !strcmp("--version", arg))
      version();
    else if (!strcmp("-A", arg) || !strcmp("--ast", arg)) {
      ast = true;
      --*argc; ++argv;
    } else if (!strcmp("-T", arg) || !strcmp("--tokens", arg)) {
      tokens = true;
      --*argc; ++argv;
    } else if ('-' == arg[0]) {
      fprintf(stderr, "unknown flag %s\n", arg);
      exit(1);
    }
  }

  return argv;
}

/*
 * Evaluate `source` with the given
 * `path` name and return status.
 */
int eval(UText* source, char const* path) {
  yt_lexer_t lex = yt_lexer_init(source, path);
  // yt_parser_t parser = yt_parser_init(&lex);
  // yt_block_node_t *root;

  // --tokens
  if (tokens) {
    while (yt_scan(&lex)) {
      printf("  \e[90m%d : \e[m", lex.lineno);
      yt_token_inspect(&lex.tok);
    }
    return 0;
  }
  /* TO BE FINISHED
  // oh noes!
  if (!(root = yt_parse(&parser))) {
    yt_report_error(&parser);
    return 1;
  }

  // --ast
  yt_set_prettyprint_func(printf);
  yt_prettyprint((yt_node_t *) root);

  // evaluate
  // yt_vm_t *vm = yt_gen((yt_node_t *) root);
  // yt_object_t *obj = yt_eval(vm);
  // yt_object_inspect(obj);
  // yt_object_free(obj);
  // yt_vm_free(vm);
  TO BE FINISHED */
  return 0;
}

/*
 * Start Yantra, parse args, and evaluate Yantra files.
 */
int main (int argc, char* argv[argc+1]) {
  char const* path;
  UText* source;

  argv = parse_args(&argc, argv);

	if (argc == 1 && u_feof(u_finit(stdin, NULL, NULL))) {
    perror("piping is not implemented yet");
    /* source = read_until_eof(stdin); */
    /* return eval(source, "stdin"); */
    return EXIT_FAILURE;
  }

	if (argc == 1) {
    perror("repl is not implemented yet");
    /* repl(); */
    return EXIT_FAILURE;
  }

  path = argv[1];
  source = yt_file_slurp(path);

	return eval(source, path);
}
