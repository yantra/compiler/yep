/*
 * Copyright (c) 2017 Jacob Russo. All rights reserved.
 *
 * This file is part of yantra, distributed under the BSD license.
 * For full terms see the included LICENSE file.
 */
#ifndef __CLAR_TEST__
#define __CLAR_TEST__

/* Import the standard clar helper functions */
// #include "clar.h"

/* Your custom shared includes / defines here */
extern int global_test_counter;

#endif
